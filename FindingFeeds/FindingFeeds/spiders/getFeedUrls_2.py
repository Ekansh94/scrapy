# -*- coding: utf-8 -*-
import scrapy
from tldextract import extract

class GetfeedurlsSpider(scrapy.Spider):
    name = 'getFeedUrls_2'
    myUrl=''
    start_urls=[]

    def __init__(self , Url,*args , **kwargs):
        super(GetfeedurlsSpider,self).__init__(*args, **kwargs)
        self.myUrl=extract(Url).registered_domain
        self.start_urls = ['http://'+self.myUrl,'http://'+self.myUrl+'/blog','http://blog/'+self.myUrl,'http://'+self.myUrl+'/feeds']
    
    possibleUrlPaths=["atom.xml", "index.atom", "index.rdf", "index.xml","index.rss","rss.xml"]
    Feeds=set([])
    FeedList=[]
    counter=0
    track=0

    def parse(self, response):
        self.track+=1
        if(response.status<400 and self.counter<5):
            print(response)
            a=response.xpath('.//link[@type="application/rss+xml" or @type="text/xml" or @type="application/atom+xml" or @type="application/x.atom+xml" or @type="application/x-atom+xml"]/@href').extract()
            if len(a)!=0:
                self.counter+=1
                for x in a:
                    if("https.//" not in x or "http://" not in x):
                        x=response.urljoin(x)
                        yield x
                    self.Feeds.add(x)
                    print(self.Feeds)
            else:
                print("No Feeds Found for : "+response.url)
            print("End..!!")
        self.FeedList=list(self.Feeds)
        print(self.FeedList)
        if(self.track>3 and self.counter==0):
            if(response.status<400):
                for prefixURl in self.start_urls:
                    for suffixUrl in self.possibleUrlPaths:
                        newUrl=response.urljoin(suffixUrl)
                        yield scrapy.Request(url=newUrl,callback=self.newPossibility)
                yield self.FeedList
        elif(self.track>3 and self.counter!=0):
            yield self.FeedList

    # def spider_closed(self, spider):


    def parseRssXMLPage(self, response):
        logging.info("in second function")
        print(response.text)
        blogTitle=response.xpath('.//item/title/text()').extract()
        print("The available titles are : ")
        for titles in blogTitle:
            print(titles)

    def newPossibility(self,response):
        print(response)
        if(response.status==200 and self.counter==0):
            self.counter+=1
            self.Feeds.add(response.url)
            print(self.Feeds)
            self.FeedList=list(self.Feeds)
            print(self.FeedList)
            print("End..!!")
            yield x
        else:
            print("No Feeds Found for : "+response.url)