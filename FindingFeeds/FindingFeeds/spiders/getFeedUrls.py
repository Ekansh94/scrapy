# -*- coding: utf-8 -*-
import scrapy
# import json
import logging


class GetfeedurlsSpider(scrapy.Spider):
    name = 'getFeedUrls'
    myUrl='startupflux.com'
    start_urls = ['http://'+myUrl]



    def checkIfExists(self,response):
        print(response)
        print(type(response))
        x = response.xpath(
            './/link[@type="application/rss+xml" or @type="text/xml" or @type="application/atom+xml" or @type="application/x.atom+xml" or @type="application/x-atom+xml"]/@href').extract()
        print(x)
        if len(x)==0:
            print("Nothing Found !")
            return []
        else:
            print("Found !")
            return x

    def parse(self, response):
        print(response)
        print(response.status)
        # a = response.xpath(
        #     './/link[@type="application/rss+xml" or @type="text/xml" or @type="application/atom+xml" or @type="application/x.atom+xml" or @type="application/x-atom+xml"]/@href').extract()
        a = scrapy.Request(url=self.start_urls[0],callback=self.checkIfExists)
        print("After func :")
        print(a)
        if len(a)==0:
            print("No Rss Found")
            #newUrl=response.urljoin("")
            newUrl='http://blog.'+self.myUrl
            print(newUrl)
            #yield scrapy.Request(url=newUrl,callback=self.parse)
            yield scrapy.Request(url=newUrl,callback=self.secondFunction)
        for i in a:
            print(i)
            if (response.request.url not in i):
                i = response.urljoin(i)
                print("New URL:" + i)
            print("***End of IF Condition***")
            if "rss.xml" in i :
                yield scrapy.Request(url=i, callback=self.parseRssXMLPage)

    def parseRssXMLPage(self, response):
        logging.info("in second function")
        print(response.text)
        blogTitle=response.xpath('.//item/title/text()').extract()
        print("The available titles are : ")
        for titles in blogTitle:
            print(titles)

    def secondFunction(self,response):
        print(response)
        print(type(response))
        print("In the modified function")
  