# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from pymongo import MongoClient

class FindingfeedsPipeline(object):
	client=MongoClient("127.0.0.1",27017)
	db=client['development']
	# db.authenticate("ekansh","Reporter123#",mechanism="SCRAM-SHA-1")
	collectionName="Scrapy_Feeds"
	collection = db['test-collection']
	print("@@@@@@@@@@@@@@@@@@@@@@in pipeline section@@@@@@@@@@@@@@@@@@@@")

	def open_spider(self, spider):
		print("@@@@@@@@@@@@@@@@@@@@ Open Spider Func @@@@@@@@@@@@@@@@@@@@")
	def close_spider(self, spider):
		print("@@@@@@@@@@@@@@@@@@@@ Close Spider Func @@@@@@@@@@@@@@@@@@@@")
	def process_item(self, item, spider):
		collection.insert({ "item": "canvas", "qty": 100, "tags": ["cotton"], "size": { "h": 28, "w": 35.5, "uom": "cm" } })
		print("@@@@@@@@@@@@@@@@@@@@ Process Spider Func @@@@@@@@@@@@@@@@@@@@")
		print(item)